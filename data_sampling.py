import pywt

def reduce_dwt(vector,wave):
    return pywt.dwt(vector,wave,'symmetric')

def invert_dwt(reduced_vector,wave='db14',times=1):
    
    for i in range(times):
        reduced_vector = pywt.idwt(reduced_vector, cD=None,wavelet=wave, mode='symmetric', axis=-1)

    return reduced_vector

def reduce_data(data,times = 1):
    wave = 'db18'

    for i in range(times):
        coe_approximation,coe_details = reduce_dwt(data,wave)

    return coe_approximation
