import build_data as bd
import data_sampling as ds
from keras.models import load_model
import numpy as np
import os
import copy
from keras import backend as K
from sklearn.metrics import f1_score,classification_report,confusion_matrix

os.environ["CUDA_VISIBLE_DEVICES"]="0"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def transform_solution(predict,desagg,window_size):
	return b2uild_windows(predict,window_size),b2uild_windows(desagg,window_size)

def b2uild_windows(time_serie,window_size):

	windows_Y = []


	for i in range(0,len(time_serie),window_size):  # Building windows
		windows_Y.append(sum(time_serie[i:i+window_size])/window_size)

	return windows_Y

def mish(inputs):
        return inputs * K.tanh(K.softplus(inputs))

def test_acc(y_hat,y,transform=True):
	window_size = 400

	if transform:
		y_hat,y= transform_solution(y_hat,y,window_size)
	
	dif = 0
	den = 0
	for i in range(np.shape(y_hat)[0]):
		dif += abs(y_hat[i] - y[i])
		den+=y[i]

	return 1 - dif/(2*den)


def calc_f1(y_hat,y,transform,appliance):
	window_size = 400

	if appliance == 'microwave':
		thrsh = 50
		#plt.savefig('microwave_notTransfrm.pdf')
	elif appliance == 'fridge':
		thrsh = 40
		#plt.savefig('fridge_notTransfrm.pdf')
	elif appliance == 'dish washer':
		thrsh = 10
	elif appliance == 'kettle':
		thrsh = 10
		#plt.savefig('kettle_notTransfrm.pdf')
	elif appliance == 'washing machine':
		thrsh = 20
	else:
		print("Error! thrsh is None! Appliance:", appliance)

	if transform:
		y_hat,y= transform_solution(y_hat,y,window_size)

	y_hat_label = []
	y_label = []
	for i in range(np.shape(y_hat)[0]):
		
		if y_hat[i] > thrsh:
			y_hat_label.append(1)
		else:
			y_hat_label.append(0)

		if y[i] > thrsh:
			y_label.append(1)
		else:
			y_label.append(0)
	tn, fp, fn, tp = confusion_matrix(y_label,y_hat_label).ravel()

	print(tp,fp,'\n',fn,tn)
	print(classification_report(y_label,y_hat_label))
	return f1_score(y_label,y_hat_label)

def metricsMAE(y_pred,y_true):
    
    mae = 0
    
    for i in range(min(len(y_true),len(y_pred))):
        mae += abs(y_true[i]-y_pred[i])

    return mae/len(y_pred)

def metricSAE(y_pred,y_true):

    return abs(sum(y_true) - sum(y_pred))/max(sum(y_true),sum(y_pred))

def des_normalize(time_serie,minValue,maxValue):

	for i in range(len(time_serie)):
		time_serie[i] = time_serie[i]*(maxValue-minValue) + minValue


	return time_serie


def normalize_data(dict_data):

	values_normalize = {}

	for key in dict_data:
	
		time_serie = list(dict_data[key])
		maxValue = max(time_serie)
		minValue = min(time_serie)
		
		values_normalize[key] = [maxValue,minValue]

		for i in range(len(time_serie)):
			
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)
		
		dict_data[key] = time_serie

	return dict_data,values_normalize


def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []
	

	for i in range(0,len(data["Aggregate"]),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		
		'''
		auxList = []
		for j in range(window_size):
			auxList.append([data['Aggregate'][i+j],data['Timetable'][i+j]])
		'''
		windows_X.append(data['Aggregate'][i:i+window_size])
		#windows_Y.append(data["Desaggregate"][i:i+window_size])
		windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])

	print(np.shape(windows_Y))
	print(np.shape(windows_X))
	#exit()
	if np.shape(windows_Y)[0] > 0:
		return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)
	#return np.array(windows_X),np.array(windows_Y)
	return windows_X,windows_Y


def buildConsumptionCurve(predict):

    curve = []

    rate = [1,1,1]

    curve.append(predict[0][0])
    curve.append(predict[0][1])
    curve.append(predict[0][2])

    for i in range(1,np.shape(predict)[0]-1):

        curve[i] = (curve[i] + rate[1]*predict[i][0])/2
        curve[i+1] = (curve[i+1]+rate[2]*predict[i][1])/2

        curve.append(predict[i][2])

    del curve[0]
    del curve[-1]

    return curve

def evaluate(appliance,model,testX,data,values_normalize,window_size_reduced,batch_size,delay_window):

	predict = model.predict(testX,batch_size=batch_size)

	predict = buildConsumptionCurve(predict)

	data['Desaggregate'] = data['Desaggregate'][int(window_size_reduced):- int(window_size_reduced)]
	
	data['Aggregate'] = data['Aggregate'][int(window_size_reduced/2):- int(window_size_reduced/2)]

	
	predict = des_normalize(predict,values_normalize['Desaggregate_wave'][1],values_normalize['Desaggregate_wave'][0])
	data['Desaggregate'] = des_normalize(data['Desaggregate'],values_normalize['Desaggregate'][1],values_normalize['Desaggregate'][0])
	data['Aggregate'] = des_normalize(data['Aggregate'],values_normalize['Aggregate'][1],values_normalize['Aggregate'][0])



	data['Aggregate'] = ds.invert_dwt(data['Aggregate'],'db18',1)[:-1]
	predict = ds.invert_dwt(predict,'db18',1)[:-1]
	
	for i in range(len(predict)):
		if predict[i] < 0:
			predict[i] = 0

	mae = metricsMAE(y_pred=predict,y_true=data['Desaggregate'])
	sae = metricSAE(y_pred=predict,y_true=data['Desaggregate'])
	f1,ea = calc_f1(predict,data['Desaggregate'],True,appliance),test_acc(predict,data['Desaggregate'],True)


	return mae,sae,rmse,nrmse,f1,ea,predict,data['Aggregate']

def main(model, window_size,appliance,times_reduction,dataset='ukdale'):

	if dataset == 'ukdale':
		data = bd.get_data_andMain(appliance,house=2,sample1minute=True)
		dataTest = {}
		dataTest['Aggregate'] = data['Aggregate'][175010:]
		dataTest[appliance] = data[appliance][175010:]

		del data['Aggregate'][175010:]
		del data[appliance][175010:]
	else:
		print("Dataset not exist")
		exit()

	dataTest["Desaggregate"] = dataTest.pop(appliance)

	#Reduced data to 1/2^TIMES_REDUCTION of the your lenght originally utilizing wavelet transform. 
	dataTest['Aggregate'] = ds.reduce_data(copy.copy(dataTest['Aggregate']),times = times_reduction)
	dataTest['Desaggregate_wave'] = ds.reduce_data(copy.copy(dataTest['Desaggregate']),times = times_reduction)

	#Eliminate the values < 0
	dataTest['Aggregate'] = list(map(lambda x: 0 if x < 0 else x,dataTest['Aggregate']))


	dataTest,values_normalize = normalize_data(dataTest)

	testX,testY = build_windows(dataTest,window_size,pass_ = 1) #Build windows to train neural network.

	return evaluate(appliance,model,testX,dataTest,values_normalize,window_size,batch_size=256,delay_window=1)

if __name__ == '__main__':
	
	apps = [
	'dish washer',
	'fridge',
	'kettle',
	'washing machine',
	'washer dryer'
	'microwave',]

	for appliance in apps:
		times_reduction = 1
		mae_list,sae_list,rmse_list,nrmse_list,f1_list,ea_list = [],[],[],[],[],[]
		mae_list_std,sae_list_std,rmse_list_std,nrmse_list_std,f1_list_std,ea_list_std = [],[],[],[],[],[]
		for window_size in [400]:
			qt = 10
			mae,sae,rmse,nrmse,f1,ea = [],[],[],[],[],[]
			for i in range(qt):
				model = load_model(dir_path+os.sep+'models'+os.sep+'best_model_'+appliance+'_Wavelet_'+str(times_reduction)+'#'+str(i)+'_W'+str(window_size)+'.h5',custom_objects={'mish':mish})
				a,b,c,d,f,g,signal,aggr = main(model, window_size=window_size,appliance=appliance,times_reduction=times_reduction)
				total_signal = [x+y for x,y in zip(total_signal,signal)]
				mae.append(a)
				sae.append(b)
				rmse.append(c)
				nrmse.append(d)
				f1.append(f)
				ea.append(g)

			f1_list.append(np.mean(f1))
			ea_list.append(np.mean(ea))
			rmse_list.append(np.mean(rmse))
			nrmse_list.append(np.mean(nrmse))
			mae_list.append(np.mean(mae))
			sae_list.append(np.mean(sae))

			f1_list_std.append(np.std(f1))
			ea_list_std.append(np.std(ea))
			rmse_list_std.append(np.std(rmse))
			nrmse_list_std.append(np.std(nrmse))
			mae_list_std.append(np.std(mae))
			sae_list_std.append(np.std(sae))

		print(f1_list)
		print(f1_list_std)
		print(ea_list)
		print(ea_list_std)
