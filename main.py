import nn as nn
import build_data as bd
import data_sampling as ds
from keras.optimizers import Adam,SGD
import numpy as np
import pandas as pd
from Early_Stopping import Early_Stopping
from keras.models import load_model
import os

import time
from keras import backend as K
import tensorflow as tf

os.environ["CUDA_VISIBLE_DEVICES"]="1"
dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def mish(inputs):
        return inputs * K.tanh(K.softplus(inputs))
def normalize_data(dict_data):

	for key in dict_data:
	
		time_serie = list(dict_data[key])
		
		maxValue = max(time_serie)
		minValue = min(time_serie)

		for i in range(len(time_serie)):
			time_serie[i] = (time_serie[i]-minValue)/(maxValue-minValue)

		dict_data[key] = time_serie

	return dict_data

def build_windows(data,window_size,pass_):

	windows_X = []
	windows_Y = []
	

	for i in range(0,int(len(data["Aggregate"])),pass_):  # Building windows
		
		if i+window_size > len(data["Aggregate"]):
			break
		
		windows_X.append(data['Aggregate'][i:i+window_size])
		windows_Y.append([ data["Desaggregate"][i+int(window_size/2)-1],data["Desaggregate"][i+int(window_size/2)],data["Desaggregate"][i+int(window_size/2)+1]])


	return np.array(windows_X).reshape(np.shape(windows_X)[0],np.shape(windows_X)[1],1),np.array(windows_Y)


def train_nn(number,model,trainX,trainY,testX,testY,history,appliance,batch_size,epochs,early,times_reduction):

	opt = Adam()
	model.compile(optimizer=opt, loss='mse')

	if early == None:
		early_stopping = Early_Stopping(max_epochs = epochs+1,min_loss = model.evaluate(np.array(testX), np.array(testY)))
	else:
		early_stopping = Early_Stopping(max_epochs = early,min_loss = model.evaluate(np.array(testX), np.array(testY)))

	for epoch in range(epochs):
		
		current_loss = model.fit(x=trainX,y=trainY,batch_size=batch_size,verbose=2,epochs=1)
		current_valLoss = model.test_on_batch(x = testX,y = testY)

		history['loss'].append(current_loss.history['loss'][0])
		history['val_loss'].append(current_valLoss)

		if early_stopping.count_loss(loss = current_valLoss,model = model, path_save = dir_path+'/models/best_model_'+appliance+'#'+str(number)+'_H2.h5'):
			print("Val_loss do not improved in {0} epochs".format(early_stopping.max_epochs))
			print("Epoch",epoch,"of",epochs)
			print("Stopping ...")
			break

	
	return history,model



def main(number,model,times_reduction, window_size, appliance,house,batch_size,epochs,early,dataset='ukdale'):
	window_size_reduced = window_size

	'''
	Build or load model
	'''
	if model == None:
		model = nn.build_architect(window_size_reduced,features=1,arch=4) #Construct neural network without compile
	else:
		model = load_model(dir_path+'/models/best_model_'+appliance+'#'+str(number)+'.h5',custom_objects={'mish':mish})
	
	'''
	Get data. 
	It's return the dictionary that contais aggregate appliance and desaggregate data
	'''
	if dataset == 'ukdale':
		data = bd.get_data_andMain(appliance,house,sample1minute=True)
	else:
		print("Dataset not exist")
		exit()

	#Rename key, appliance to Desaggregate
	data["Desaggregate"] = data.pop(appliance)

	if dataset == 'ukdale':
		dataTest = bd.get_data_andMain(appliance,house=1,sample1minute=True)
		dataTest["Desaggregate"] = dataTest.pop(appliance)
		
		#Split data for test
		dataTest['Aggregate'] = dataTest['Aggregate'][581891:706227]
		dataTest['Desaggregate'] = dataTest['Desaggregate'][581891:706227]

	
	'''
	Reduced data to 1/2^times_reduction of the your 
	lenght originally utilizing wavelet transform. 
	'''
	dataTest['Aggregate'] = ds.reduce_data(dataTest['Aggregate'],times = times_reduction)
	dataTest['Desaggregate'] = ds.reduce_data(dataTest['Desaggregate'],times = times_reduction)
	
	data['Aggregate'] = ds.reduce_data(data['Aggregate'],times = times_reduction)
	data['Desaggregate'] = ds.reduce_data(data['Desaggregate'],times = times_reduction)
	

	#Eliminate the values < 0
	dataTest['Aggregate'] = list(map(lambda x: 0 if x < 0 else x,dataTest['Aggregate']))
	dataTest['Desaggregate'] = list(map(lambda x: 0 if x < 0 else x,dataTest['Desaggregate']))

	data['Aggregate'] = list(map(lambda x: 0 if x < 0 else x,data['Aggregate']))
	data['Desaggregate'] = list(map(lambda x: 0 if x < 0 else x,data['Desaggregate']))

	#Normalize the data to a range from 0 to 1
	data = normalize_data(data)
	dataTest = normalize_data(dataTest)

	'''
	Build windows for TEST and TRAIN.
	'''
	testX,testY = build_windows(dataTest,window_size_reduced,pass_ = 1) #Build windows to train neural network.
	trainX,trainY = build_windows(data,window_size_reduced,pass_ = 1) #Build windows to train neural network.


	history = {'loss':[],'val_loss':[]}

	'''
	Train model
	'''
	history,model = train_nn(number,model,trainX,trainY,testX,testY,history,appliance,batch_size,epochs,early,times_reduction)
	pd.DataFrame(history).to_csv('history_'+appliance+'.csv')
	K.clear_session()
	tf.keras.backend.clear_session()

	del dataTest
	del data
	del trainX,trainY,testX,testY,history

	return model

if __name__ == '__main__':

	apps = {
	'dish washer':[5,1],
	'fridge':[5,1],
	'kettle':[5,1],
	'microwave':[5,1],
	'washing machine':[4]}

	b = time.time()
	for app in apps:
		for i in range(10):
			for house_ind in range(len(apps[app])):
				for win_size in [400]:
					if house_ind == 0:
						m = main(i,None,times_reduction=1, window_size=win_size, appliance=app,house=apps[app][house_ind],batch_size=256,epochs=3000,early=250,dataset='ukdale')
					else:
						m = main(i,'na',times_reduction=1, window_size=win_size, appliance=app,house=apps[app][house_ind],batch_size=256,epochs=3000,early=250)
	
	

