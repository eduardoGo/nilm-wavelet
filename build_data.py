import pandas as pd
import os

dir_path = str(os.path.dirname(os.path.realpath(__file__)))

def get_data_andMain(appliance, house, sample1minute=False, time=False):
    data = {}
    if sample1minute:
        location = dir_path + os.sep + 'data' + os.sep + 'ukdale' + os.sep + 'House' + str(house) + os.sep + str(
            appliance) + 'AndMainComplete_1minute.csv'
    else:
        location = dir_path + '/data/ukdale/House' + str(house) + '/' + str(appliance) + 'AndMainComplete.csv'

    df_data = pd.read_csv(location)

    data[appliance] = list(map(float, df_data['Desaggregate'].values))
    data['Aggregate'] = list(map(float, df_data['Aggregate'].values))

    if time:
        data["Time"] = list(df_data['Time'].values)

    return data
