from keras.layers import Conv1D,Dense,Flatten,Input
from keras.models import Model
from keras import backend as K

def mish(inputs):
	return inputs * K.tanh(K.softplus(inputs))

def build_architect(window_size,features):


	inputs = Input(shape=(window_size,1))
			
	x = Conv1D(kernel_size=12,filters=20,activation=mish)(inputs)

	x = Conv1D(kernel_size=12,filters=20,activation=mish)(x)

	x = Conv1D(kernel_size=12,filters=20,activation=mish)(x)

	x = Conv1D(kernel_size=9,filters=15,activation=mish)(x)

	x = Conv1D(kernel_size=9,filters=10,activation=mish)(x)

	x = Conv1D(kernel_size=9,filters=7,activation=mish)(x)
		
	x = Flatten()(x)
	x = Dense(512,activation=mish)(x)
		
	outputs = Dense(3,activation='linear')(x)
	model = Model(inputs,outputs)

	return model






